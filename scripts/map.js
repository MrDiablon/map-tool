let map;
let polylines = [];

$(() => {
    $('#reset').on('click', function() {
        console.log('reset');
        polylines.forEach(elt => {
            elt.setMap(null);
        });
    });
});

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 49.2882, lng: 4.00891 },
        zoom: 18,
    });
}

function addPolyline() {
    var lat1 = Number($('#lat1').val());
    var lng1 = Number($('#lng1').val());
    var lat2 = Number($('#lat2').val());
    var lng2 = Number($('#lng2').val());

    console.log(lat1, lng1, lat2, lng2);

    console.log;
    var poly = new google.maps.Polyline({
        path: [{ lat: lat1, lng: lng1 }, { lat: lat2, lng: lng2 }],
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2,
    });

    poly.setMap(map);

    polylines.push(poly);

    map.panTo(new google.maps.LatLng(lat1, lng1));
}

function addPolylineFromJSON() {
    var json = JSON.parse($('#json_text').val());

    if (
        json &&
        json.tabGpsPrimaire &&
        json.tabGpsSecondaire &&
        json.tabGpsPrimaire.length != 0 &&
        json.tabGpsSecondaire.length != 0
    ) {
        let primaire = json.tabGpsPrimaire;
        let secondaire = json.tabGpsSecondaire;
        let balisePeer = [];
        for (
            let i = 0;
            i < json.tabGpsPrimaire.length && i < json.tabGpsSecondaire.length;
            i++
        ) {
            console.log(primaire[i].LATITUDE);
            balisePeer[i] = [
                {
                    lat: Number(primaire[i].LATITUDE.replace('N', '')),
                    lng: Number(primaire[i].LONGITUDE.replace('E', '')),
                },
                {
                    lat: Number(secondaire[i].LATITUDE.replace('N', '')),
                    lng: Number(secondaire[i].LONGITUDE.replace('E', '')),
                },
            ];
        }

        console.table(balisePeer);

        balisePeer.forEach((element, index) => {
            polylines[index] = new google.maps.Polyline({
                path: element,
                geodesic: true,
                strokeColor: '#00FF00',
                strokeOpacity: 1.0,
                strokeWeight: 2,
            });

            polylines[index].setMap(map);
            // $('#points_list');
        });
    } else {
        console.log('not pass');
    }
}
